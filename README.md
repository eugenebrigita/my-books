[![pipeline status](https://gitlab.com/eugenebrigita/my-books/badges/master/pipeline.svg)](https://gitlab.com/eugenebrigita/my-books/commits/master)
[![coverage report](https://gitlab.com/eugenebrigita/my-books/badges/master/coverage.svg)](https://gitlab.com/eugenebrigita/my-books/commits/master)

## Eugene Brigita Lauw 
### 1806141183 | Perancangan dan Pemrograman Web 2019/1
##### The website can be accessed [here](http://eugene-books.herokuapp.com/)!

```
Introduction to:
Asynchronous Javascript and XML (AJAX)
JavaScript Object Notation (JSON)

```