from django.urls import path
from django.conf import settings
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.search, name='books')
]