function ajaxCall(value) {
    console.log(value)
    $('tbody').empty()
    let keyword = value
    $.ajax({
        method: "GET",
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + keyword,
        success: function (response) {
            for (let i = 0; i < response.items.length; i++) {
                let tempBody = document.createElement('tr')
                let tempNum = document.createElement('td')
                tempNum.innerText = i + 1
                tempBody.append(tempNum)

                let tempImage = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined) {
                    let image = document.createElement('img')
                    image.src = response.items[i].volumeInfo.imageLinks.thumbnail
                    tempImage.append(image)
                } else {
                    let image = document.createElement('img')
                    image.alt = "No Cover Available"
                    tempImage.append(image)
                }

                let tempTitle = document.createElement('td')
                tempTitle.innerText = response.items[i].volumeInfo.title

                let tempAuthor = document.createElement('td')
                if (response.items[i].volumeInfo.authors !== undefined) {
                    for (let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                        tempAuthor.innerText = response.items[i].volumeInfo.authors[j]
                    }
                } else {
                    tempAuthor.innerText = 'Author(s) Unknown'
                }

                // let tempDetails = document.createElement('td')
                // if (response.items[i].volumeInfo.description !== undefined) {
                //     tempDetails.innerText = response.items[i].volumeInfo.description
                // } else {
                //     tempDetails.innerText = 'No further description :('
                // }

                tempBody.append(tempImage, tempTitle, tempAuthor)
                $('tbody').append(tempBody)
            }
        },
        error: function() {
            console.log('Failed')
        }
    })
}

let button = $('#search-button')
let container = $('.content-container')

$(document).ready(ajaxCall('passion'))

button.on('click', function() {
    $('.table-content').show()
    let keyword = $('#search-field').val()
    ajaxCall(keyword)
})