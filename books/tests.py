from django.test import TestCase, Client
from django.urls import resolve
from .views import search

# Create your tests here.
class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_views_func_in_used(self):
        response = resolve('/')
        self.assertEqual(response.func, search)

    def test_book_html_is_used(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('book.html')

    def test_search_box_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf-8')
        self.assertIn('form-control', content)

    def test_search_button_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf-8')
        self.assertIn('btn btn-outline-light', content)

    def test_table_content_is_exist(self):
        response = Client().get('/')
        content = response.content.decode('utf-8')
        self.assertIn('table-content', content)